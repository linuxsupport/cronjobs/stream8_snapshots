job "${PREFIX}_stream8_snapshots" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = false
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_stream8_snapshots" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/stream8_snapshots/stream8_snapshots:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_stream8_snapshots"
        }
      }
      volumes = [
        "$DATA:/data",
        "$ADVISORIES:/advisories",
        "/etc/nomad/submit_token:/secrets/token",
        "/etc/nomad/ssh-id_ed25519:/root/.ssh/id_ed25519",
        "/etc/nomad/linuxci_api_token:/secrets/linuxci_api_token",
        "/etc/nomad/linuxci_pw:/secrets/linuxci_pw",
        "/etc/nomad/rundeck_api_token:/secrets/rundeck_api_token"
      ]
    }

    env {
      PATH_DESTINATION = "$PATH_DESTINATION"
      PRODDATE = "$PRODDATE"
      OLDESTDATE = "$OLDESTDATE"
      EMAIL_FROM = "$EMAIL_FROM"
      EMAIL_ADMIN = "$EMAIL_ADMIN"
      EMAIL_USERS = "$EMAIL_USERS"
      EMAIL_USERS_TEST = "$EMAIL_USERS_TEST"
      NOMAD_ADDR = "$NOMAD_ADDR"
    }

    resources {
      cpu = 6000 # Mhz
      memory = 1024 # MB
    }

  }
}
