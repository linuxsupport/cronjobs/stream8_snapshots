#!/bin/bash

source /root/common.sh

echo "Checking for dangerous packages..."

TMPDIR=`mktemp -d`

find "$DEST" -not -path '*/CERN/*' -not -path '*/Source/*' -not -path '*/Debug/*' -name .diff -exec cat {} \; | sort -u  > $TMPDIR/alldiffs

while IFS= read -r LINE; do
  [[ -z "$LINE" ]] && continue
  while IFS= read -r RPM; do
    [ $RPM ] || continue
    FILE="`echo $RPM | cut -d';' -f 1`"
    N="`echo $RPM | cut -d';' -f 2`"
    A="`echo $RPM | cut -d';' -f 5`"
    echo "Dangerous package $FILE found in diff"

    [[ "$A" == "noarch" ]] && ARCHES="$SUPPORTED_ARCHES" || ARCHES="$A"

    for ARCH in $ARCHES; do
      # Assemble all the repos into a list of --repofrompath parameters for repoquery
      repos="`find "$DEST" -path "*/${ARCH}/*" -not -path '*/Source/*' -not -path '*/Debug/*' -not -path '*/.clean.*' -name 'repodata' | sed 's#/repodata##' | awk '{printf " --repofrompath=repo" NR "," $0}'`"

      latest_all=`repoquery --disablerepo='*' $repos --show-duplicates --qf="%{name}-%{version}-%{release}" $N 2>/dev/null | sort -V | tail -n1`
      latest_cern=`repoquery --disablerepo='*' --repofrompath=repo17,${DEST}/CERN/${ARCH}/ --show-duplicates --qf="%{name}-%{version}-%{release}" $N 2>/dev/null | sort -V | tail -n1`

      if [[ "$latest_all" == "$latest_cern" ]]; then
        echo " -> Newest is from the CERN repo, can continue."
      else
        echo " -> No equivalent CERN package found!"
        DANGER="${DANGER}${FILE} "
      fi
    done

  done <<< "`grep ";$LINE;" $TMPDIR/alldiffs`"
done <<< "`echo "$DANGEROUS_RPMS" | tr ' ' '\n'`"

rm -rf $TMPDIR

if [ "$DANGER" != "" ]; then
  DANGER=`printf "$DANGER" | sort -u | cut -d ';' -f 1`
  echo "Found some dangerous RPMs, creating .freeze.${RELEASE}all"
  touch -a "$DESTINATION/.freeze.${RELEASE}all"

  export PACKAGES="`echo -n "$DANGER" | tr ' ' '\n'`"
  export RELEASE=$RELEASE
  envsubst < templates/email_tripwire.tpl | swaks --server $MAILMX --to $EMAIL_ADMIN --data -
fi
