#!/bin/bash

source /root/common.sh

# Rather than introduce a new variable, test if $DESTINATION is not prod
if [ "$DESTINATION" != "/data/cern/centos" ]; then
  echo "On test environment, do not push updates to repo"
  exit 0
fi

FINALDEST=$1
CHANNEL=$2
BRANCH='master'

ADVURL="https://access.redhat.com/errata/"
CVEURL="https://access.redhat.com/security/cve/"

case $CHANNEL in
  "testing")
    REPO_DEST_PATH="updates/c${RELEASE}/test/"
    ;;
  "production")
    REPO_DEST_PATH="updates/c${RELEASE}/prod/"
    ;;
  *)
    echo "CHANNEL not specified, no commiting"
    exit 0
    ;;
esac

for ARCH in $SUPPORTED_ARCHES; do
  for REPO in $ALL_REPOS; do
    REPO_DIFF=$FINALDEST/.diff:${REPO}:${ARCH}

    # If there's no diff, there's nothing to do
    [ -s "$REPO_DIFF" ] || continue

    echo "### $REPO $ARCH repository" >> $FINALDEST/.updates
    echo >> $FINALDEST/.updates
    echo "Package | Version | Advisory | Notes" >> $FINALDEST/.updates
    echo "------- | ------- | -------- | -----" >> $FINALDEST/.updates
    for LINE in `cat $REPO_DIFF`; do
      # LOS-483 Do not include i686 packages in the updates
      P_ARCH=`echo $LINE | awk -F\; '{print $5}'`
      [ "$P_ARCH" == "i686" ] && continue

      # Split table variables to ease future css styling if needed, much more readable than one-liners
      PACKAGE=`echo $LINE | awk -F\; '{print $2}'`
      # Include version and release
      VERSION=`echo $LINE | awk -F\; '{printf "%s-%s", $3,$4}'`
      ADVTYPE=`echo $LINE | awk -F\; '{print $6}'`
      ADVISORIES=`echo $LINE | awk -F\; '{print $7}'`
      CVES=`echo $LINE | awk -F\; '{print $8}'`

      ADV=$(echo $ADVISORIES | sed 's/,/\n/g' | sed "s|\(.*\)|[\1]($ADVURL\1)|" | sed ':a; N; $!ba; s/\n/, /g')

      if [ ! -z $ADVTYPE ]; then
        # text type might be just "security" so we specify the text to insert
        if [ "$ADVTYPE" == "S" ]; then
          ADVTYPE_HTML='<div class="adv_s">Security Advisory</div>'
        elif [ "$ADVTYPE" == "B" ]; then
          ADVTYPE_HTML='<div class="adv_b">Bug Fix Advisory</div>'
        elif [ "$ADVTYPE" == "E" ]; then
          ADVTYPE_HTML='<div class="adv_e">Product Enhancement Advisory</div>'
        else
          ADVTYPE_HTML='<div class="adv_e">[?]</div>'
        fi

        if [ ! -z $CVES ]; then
          # Note the | in the second sed. $CVEURL contains /, so we have to use a different separator
          CVES=$(echo $CVES | sed 's/,/\n/g' | sed "s|\(.*\)|[\1]($CVEURL\1)|" | sed ':a; N; $!ba; s/\n/, /g')
          ADVTYPE_HTML="${ADVTYPE_HTML} (${CVES})"
        fi

        echo "$PACKAGE | $VERSION | $ADV | $ADVTYPE_HTML" >> $FINALDEST/.updates
      else
        echo "$PACKAGE | $VERSION | |" >> $FINALDEST/.updates
      fi

    done
    echo >> $FINALDEST/.updates

  done
done

if [ ! -s "$FINALDEST/.updates" ]; then
  echo "There are no updates, no need to commit a file"
  exit 0
fi

# Add the header to the beginning of the file
# Never use first level header or mkdocs will not build table of contents with multiple h1 in the same page
sed -i "1i## $TODAY_FORMAT\n" $FINALDEST/.updates

# Through volumes, nomad will have the required ssh key for git interaction
git config --global user.name "CERN Linux Droid"
git config --global user.email "linux.ci@cern.ch"

# We use ssh for git, therefore we add gitlab to known_hosts for non interactive git commands
ssh-keyscan -t rsa -p 7999 gitlab.cern.ch >> ~/.ssh/known_hosts

# We use ssh, nomad mounts the ssh key secret (coming from teigi) for this to work
rm -rf /documentation
git clone --depth 1 ssh://git@gitlab.cern.ch:7999/linuxsupport/websites/linux.cern.ch.git /documentation
cd /documentation
git checkout $BRANCH

mkdir -p /documentation/docs/$REPO_DEST_PATH
cat $FINALDEST/.updates >> /documentation/docs/$REPO_DEST_PATH/$TODAY.md
git add /documentation/docs/$REPO_DEST_PATH/$TODAY.md

git commit -m "CS8 $CHANNEL updates $TODAY_FORMAT"

# Disable manual edit of commit message so it automerges
export GIT_MERGE_AUTOEDIT=no
# Merge made by the 'recursive' strategy. It does not conflict with other jobs
git pull --rebase
git push origin $BRANCH

rm $FINALDEST/.updates

