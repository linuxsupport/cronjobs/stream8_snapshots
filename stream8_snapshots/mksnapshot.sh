#!/bin/bash

source /root/common.sh

echo "Creating snapshot for $TODAY"

ADVISORY_DB_PATH="/advisories/c8X"

# NOTE: we don't sync aarch64 from RedHat, hence we don't have an updateinfo.xml available
# [build@lxsoftadm01] ls  /mnt/data1/dist/cdn.redhat.com/content/dist/rhel8/8/
#    x86_64
# the errata entries will be the same in any case as the x86_64 version
# For now, we use the same errata links for both archs
# Use https://www.sqlite.org/uri.html for read-only mode, use .db.bak if existent to avoid deadlocks with https://gitlab.cern.ch/linuxsupport/cronjobs/advisories
if [ -f "$ADVISORY_DB_PATH/x86_64/updateinfo.db.bak" ]; then
  echo "Advisories generation is currently running, use backup file to avoid deadlocks"
  SQLITE="/usr/bin/sqlite3 file:$ADVISORY_DB_PATH/x86_64/updateinfo.db.bak?mode=ro"
else
  echo "Advisories generation is NOT running, using main file"
  SQLITE="/usr/bin/sqlite3 file:$ADVISORY_DB_PATH/x86_64/updateinfo.db?mode=ro"
fi

addAdvisory() {
  local DIFF_FILE=$1
  for LINE in $(cat $DIFF_FILE); do
    # LOS-969 The dist tag keeps us from matching some packages in the advisory db, so filter it (but keep the .el in the package name so we don't match incorrect things)

    local NVR_NODIST=$(echo $LINE | awk -F\; '{print $1}' | sed 's/\.[^.]\+\.rpm$//' | sed 's/\.el[^.]\+/.el/')
    local ADVID=''
    local ADVTYPE=''
    local CVES=''

    for ADV in $($SQLITE 'select distinct advisory, file from erratapackages where file like "'$NVR_NODIST'%" order by id desc'); do
      local ADVNAME=$(echo $ADV | awk -F\| '{print $1}')
      local ADVFILE_NODIST=$(echo $ADV | awk -F\| '{print $2}' | sed 's/\.[^.]\+\.rpm$//' | sed 's/\.el[^.]\+/.el/')
      # We can't use regepxs in sqlite, so we have to filter the results here
      # RHEL produces packages with "dist tags" like ".el9_0.1", which should not be matched with a ".el9" package
      [ "$ADVFILE_NODIST" != "$NVR_NODIST" ] && continue

      local CVE_LIST=$($SQLITE 'select group_concat(distinct cve_id) from erratacves where advisory="'$ADVNAME'" order by cve_id')

      local TYPE=$($SQLITE 'select type from errata where advisory="'$ADVNAME'"')
      # ADVTYPE should be the highest priority advisory, if there are multiple.
      if [ "$TYPE" == "Security Advisory" ] || [ "$TYPE" == "security" ]; then
        ADVTYPE='S'
      elif [ "$TYPE" == "Bug Fix Advisory" ] || [ "$TYPE" == "bugfix" ]; then
        [[ "$ADVTYPE" == "S" ]] && continue
        ADVTYPE='B'
      elif [ "$TYPE" == "Product Enhancement Advisory" ] || [ "$TYPE" == "enhancement" ]; then
        [[ "$ADVTYPE" == "S" || "$ADVTYPE" == "B" ]] && continue
        ADVTYPE='E'
      fi

      ADVID="$ADVID,$ADVNAME"
      [ ! -z "$CVE_LIST" ] && CVES="$CVES,$CVE_LIST"
    done

    # Sort and remove duplicates (plus gets rid of the leading comma)
    ADVID=$(echo $ADVID | tr ',' '\n' | sort -u | xargs | sed 's/ /,/g')
    CVES=$(echo $CVES   | tr ',' '\n' | sort -u | xargs | sed 's/ /,/g')

    # Find and replace, adding errata info, use # as separator cannot be on the line replacement
    sed -i "s#$LINE#$LINE;$ADVTYPE;$ADVID;$CVES#g" $DIFF_FILE
  done
}

# Clean up rsync temporary files
cleanUp() {
  find $1 -name '.~tmp~' -exec rm -rv {} \; 2>/dev/null
}

# If we're going to kludge a repository, save a clean version first.
kludgeBackup() {
  local REPO="$DEST/$1"
  local BACKUP="`dirname $DEST/.clean.$1`"

  [[ -d "$BACKUP" ]] && return

  mkdir -pv $BACKUP
  cp -Rl $REPO $BACKUP
}

# Look for packages in our filter list and remove them if necessary.
# Grab packages from our include list and add them to the snapshot.
kludgeRepository() {
  local REPO="$DEST/$1"
  local PREV="$SNAPS/$YESTERDAY/$1"

  local DIRTY=0
  while IFS= read -r RPM; do
    [[ -z "${RPM}" ]] && continue
    local FILE="`find "${REPO}" -name "${RPM}"`"

    if [[ -n "${FILE}" ]]; then
      # Back up what we have before we screw with it
      kludgeBackup $1

      echo "Found ${RPM} at ${FILE}"
      echo "${FILE}" | xargs rm -v
      DIRTY=1
    else
      echo "No matches for: find "${REPO}" -name "${RPM}""
    fi
  done <<< "`sed 's/^\s*//;s/\s*$//;s/\s*#.*$//;/^\s*$/d' /root/packages_filtered.lst`"
  # Sed cleans up our list of filtered packages by removing leading/trailing spaces, comments and empty lines

  while IFS= read -r LINE; do
    [[ -z "$LINE" ]] && continue
    local LINE="${LINE//\$ARCH/$ARCH}"
    local repo="`echo "${LINE}" | cut -d ';' -f 1`"

    # Check what repo we're in, stripping final slashes just in case
    [[ "${1%/}" != "${repo%/}" ]] && continue

    local RPM="`echo "${LINE}" | cut -d ';' -f 2`"
    echo "Looking to include ${RPM}"

    local dir="`dirname "/data/${RPM}"`"
    local filename="`basename "/data/${RPM}"`"

    local FILE="`find "$dir" -name "$filename"`"

    if [[ -n "${FILE}" ]]; then
      # Back up what we have before we screw with it
      kludgeBackup $1

      echo "Found ${FILE}"
      echo "${FILE}" | xargs -I{} cp -vl {} $REPO/Packages/
      DIRTY=1
    fi
  done <<< "`sed 's/^\s*//;s/\s*$//;s/\s*#.*$//;/^\s*$/d' /root/packages_included.lst`"

  # If we modified something, we need to regenerate the repodata
  if [[ $DIRTY -eq 1 ]]; then
    # Look for comps files, we need them for the new metadata
    COMPS=`find "${REPO}" -name '*-comps-*.xml' -not -path '*.~tmp~*' -print -quit`
    if [[ -n "${COMPS}" ]]; then
      COMPS="-g ${COMPS}"
    fi

    # We're about the change the repodata, so the signature won't be valid anymore
    rm -vf "${REPO}/repodata/repomd.xml.asc"

    echo "Packages kludged, regenerating metadata"
    /usr/bin/createrepo \
      --workers 5 \
      --xz \
      --update \
      --keep-all-metadata \
      $COMPS \
      --outputdir $REPO \
      $REPO

    # Now let's see which files haven't changed and we can hard-link again
    while IFS= read -r FILE; do
      if cmp --silent "${REPO}/repodata/${FILE}" "${PREV}/repodata/${FILE}"; then
        echo "${FILE} has not changed, hard-linking to previous version"
        cp -fl "${PREV}/repodata/${FILE}" "${REPO}/repodata/${FILE}"
      fi
    done <<< "`find ${REPO}/repodata/ -type f -printf '%f\n'`"
  fi
}

# Make sure we have repo metadata, and create it if necessary
checkRepodata() {
  local REPO="$DEST/$1"
  local PREV="$SNAPS/$YESTERDAY/$1"
  local DIFF="${2:-$REPO/.diff}"

  if [[ ! -d "$REPO/repodata" ]]; then
    if [[ -s "$DIFF" || ! -d "$PREV/repodata" ]]; then
      mkdir -p $REPO
      # Something changed, so we need to generate new repodata
      /usr/bin/createrepo \
        --workers 5 \
        --xz \
        --outputdir $REPO \
        $REPO
    else
      # No changes, just hardlink the previous repodata
      cp -Rl $PREV/repodata $REPO
    fi
  fi
}

# Make sure we start from a clean slate
quickDelete $TODELETE
quickDelete $DEST
mkdir -pv $DEST
echo "$TODAY" > $DEST/snapshotdate.txt
cp -l $SOURCE/COMPOSE_ID $DEST/COMPOSE_ID

for REPO in $UPSTREAM_REPOS; do
  for ARCH in $SUPPORTED_ARCHES; do
    # Create the new path
    mkdir -pv $DEST/$REPO/$ARCH/os

    # Copy stuff
    cp -Rl $SOURCE/$REPO/$ARCH/os/{Packages,repodata} $DEST/$REPO/$ARCH/os/
    cp -Rl $SOURCE/$REPO/$ARCH/os/.{disc,tree}info $DEST/$REPO/$ARCH/os/ 2>/dev/null

    # We also need the images directory of BaseOS
    if [[ "$REPO" == "BaseOS" ]]; then
      cp -Rl $SOURCE/$REPO/$ARCH/os/images $DEST/$REPO/$ARCH/os/
    fi

    # Clean up sync artifacts
    cleanUp $DEST/$REPO/$ARCH/os/

    # Remove filtered packages
    kludgeRepository $REPO/$ARCH/os/

    # Look for differences
    diffRepos $DEST/$REPO/$ARCH/os/Packages $SNAPS/$YESTERDAY/$REPO/$ARCH/os/Packages > $DEST/$REPO/$ARCH/os/.diff

    # Adding advisory information
    addAdvisory "$DEST/$REPO/$ARCH/os/.diff"

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/$ARCH/os/.diff" ] || rm -f "$DEST/$REPO/$ARCH/os/.diff"

    # If we don't have a repodata (should never happen), we will have to create it
    checkRepodata "$REPO/$ARCH/os"

    # If we already have a Source directory, it's because we copied it for a previous $ARCH, so we can continue
    [[ -d $DEST/$REPO/Source ]] && continue

    # Copy sources
    cp -Rl $SRC_RPMS/$REPO/Source $DEST/$REPO/
    cleanUp $DEST/$REPO/Source
    diffRepos $DEST/$REPO/Source/SPackages $SNAPS/$YESTERDAY/$REPO/Source/SPackages > $DEST/$REPO/Source/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/Source/.diff" ] || rm -f "$DEST/$REPO/Source/.diff"

    # If we don't have a repodata (happens sometimes), we will have to create it
    checkRepodata "$REPO/Source"
  done
done

# Now we copy the debuginfo RPMs
for ARCH in $SUPPORTED_ARCHES; do
  # Create the new path
  mkdir -pv $DEST/Debug/$ARCH

  # Copy debuginfo
  cp -Rl $DBG_RPMS/$ARCH/Packages $DEST/Debug/$ARCH
  cleanUp $DEST/Debug/$ARCH
  diffRepos $DEST/Debug/$ARCH/Packages $SNAPS/$YESTERDAY/Debug/$ARCH/Packages > $DEST/Debug/$ARCH/.diff

  # If there's no diff, just delete the file
  [ -s "$DEST/Debug/$ARCH/.diff" ] || rm -f "$DEST/Debug/$ARCH/.diff"

  # If we don't have a repodata (which we never do for Debug), we will have to create it
  checkRepodata "Debug/$ARCH"
done

# Now we copy the Koji RPMs
for REPO in $KOJI_REPOS; do
  snapshotKojiRepo $REPO
done

# Copy the SIG content
for REPO in $SIG_REPOS; do
  for ARCH in $SUPPORTED_ARCHES; do
    # Create the new path
    mkdir -pv $DEST/$REPO/Source

    for PROJECT in $(ls $SOURCE/$REPO/$ARCH/); do
      # Create the new path
      mkdir -pv $DEST/$REPO/$ARCH/$PROJECT/

      # Copy stuff
      cp -Rl $SOURCE/$REPO/$ARCH/$PROJECT/{Packages,repodata} $DEST/$REPO/$ARCH/$PROJECT/

      # Clean up sync artifacts
      cleanUp $DEST/$REPO/$ARCH/$PROJECT/

      # Remove filtered packages
      kludgeRepository $REPO/$ARCH/$PROJECT/

      # Look for differences
      diffRepos $DEST/$REPO/$ARCH/$PROJECT/Packages $SNAPS/$YESTERDAY/$REPO/$ARCH/$PROJECT/Packages >> $DEST/$REPO/$ARCH/.diff

      # If we don't have a repodata (which shouldn't happen), we will have to create it
      checkRepodata "$REPO/$ARCH/$PROJECT" "$REPO/$ARCH/.diff"

      # If we already have a Source directory, it's because we copied it for a previous $ARCH, so we can continue
      [[ -d $DEST/$REPO/Source/$PROJECT ]] && continue

      # Copy sources
      cp -Rl $SRC_RPMS/$REPO/Source/$PROJECT $DEST/$REPO/Source/
      cleanUp $DEST/$REPO/Source/
      diffRepos $DEST/$REPO/Source/$PROJECT $SNAPS/$YESTERDAY/$REPO/Source/$PROJECT > $DEST/$REPO/Source/.diff

      # If there's no diff, just delete the file
      [ -s "$DEST/$REPO/Source/.diff" ] || rm -f "$DEST/$REPO/Source/.diff"

      # If we don't have a repodata (which shouldn't happen), we will have to create it
      checkRepodata "$REPO/Source"
    done

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/$ARCH/.diff" ] || rm -f "$DEST/$REPO/$ARCH/.diff"

    # Create new path
    mkdir -pv $DEST/$REPO/Debug/$ARCH

    # Copy debuginfo
    cp -Rl $SIGDBG_RPMS/$REPO/$ARCH $DEST/$REPO/Debug/$ARCH/Packages
    cleanUp $DEST/$REPO/Debug/$ARCH/Packages
    diffRepos $DEST/$REPO/Debug/$ARCH/Packages $SNAPS/$YESTERDAY/$REPO/Debug/$ARCH/Packages > $DEST/$REPO/Debug/$ARCH/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/Debug/$ARCH/.diff" ] || rm -f "$DEST/$REPO/Debug/$ARCH/.diff"

    # If we don't have a repodata (which we never do for Debug), we will have to create it
    checkRepodata "$REPO/Debug/$ARCH"
  done
done

# Point .s8-latest to today's snapshot
cd $DESTINATION
linkTargetTo $SNAPS_DIR/.tmp.$TODAY ".${RELEASE}-latest"
/root/regen-repos.sh "c${RELEASE}-latest-.*"

echo "Sending Daily diff"

/root/sendemail.sh $DEST "daily"
