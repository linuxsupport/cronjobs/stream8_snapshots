To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: $NOPKG_SUBJECT_PREFIX: CS8 - No new packages today

Dear Linux admins,

Just a quick message to let you know that there were no new upstream CentOS Stream 8 packages today.

Here's the current state of the symlinks:

$STATE

Have a nice day!

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
