To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [CS8]: Validation of package installation failed: will continue anyway

Dear admins,

It was not possible to $VALIDATION_EMAIL_TEXT from today's snapshot. Something is wrong and needs manual investigation.
The error message whilst trying to install was:

---
--ERROR--

---

This is just a warning, the process will continue. YOLO!

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
