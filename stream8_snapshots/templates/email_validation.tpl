To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [CS8]: Validation of package installation failed: automation stopped

Dear admins,

********************************
***  Manual action required  ***
********************************

It was not possible to $VALIDATION_EMAIL_TEXT from today's snapshot. Something is wrong and needs manual investigation.
The error message whilst trying to install was:

---
--ERROR--

---

A .freeze.${RELEASE}all file has been created and links will not be updated.

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
