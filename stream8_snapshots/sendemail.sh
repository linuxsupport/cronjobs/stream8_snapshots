#!/bin/bash

source /root/common.sh

TMPDIR=$1
if [[ $2 == "daily" ]]; then
  CHANNEL="daily"
  TEMPLATE="/root/templates/email_dailydiff.tpl"
  EMAIL_TO="$EMAIL_ADMIN"
elif [[ $2 == $LN_TESTING ]]; then
  CHANNEL="testing"
  TEMPLATE="/root/templates/email_testing.tpl"
  EMAIL_TO="$EMAIL_USERS_TEST"
else
  CHANNEL="production"
  TEMPLATE="/root/templates/email_production.tpl"
  EMAIL_TO="$EMAIL_USERS"
fi


for ARCH in $SUPPORTED_ARCHES; do
  for REPO in $ALL_REPOS; do
    CLEANREPO="${REPO/\//-}"
    if [[ $CHANNEL == "daily" ]]; then
      SEARCH="-path '*/$REPO/$ARCH/*' -name .diff"
    elif [[ $CHANNEL == "testing" ]]; then
      SEARCH="-name '.diff:*-$LN_TESTING:$CLEANREPO:$ARCH'"
    else
      SEARCH="-name '.diff:*:$CLEANREPO:$ARCH' -not -name '.diff:*-$LN_TESTING:*'"
    fi
    eval find "$TMPDIR" $SEARCH -exec cat {} '\;' | sort -fuo "$TMPDIR/.diff:$CLEANREPO:$ARCH"

    # Check if .diff file is empty
    if [[ ! -s "$TMPDIR/.diff:$CLEANREPO:$ARCH" ]]; then
      rm -f "$TMPDIR/.diff:$CLEANREPO:$ARCH"
      continue
    fi

    cut -d';' -f2 "$TMPDIR/.diff:$CLEANREPO:$ARCH" >> "$TMPDIR/.longlist"

    # Log the diff for posterity
    awk -F';' -v REPO=$REPO -v ARCH=$ARCH '{print "Repodiff: "REPO" "ARCH": "$2" "$3" "$4" "$5}' "$TMPDIR/.diff:$CLEANREPO:$ARCH"
  done
done

STATE=""
while IFS= read -r LINE; do
  MODTIME="`echo $LINE | cut -d'.' -f1`"
  LINK="`echo $LINE | cut -d' ' -f2`"
  TARGET="`readlink $DESTINATION/$LINK`"
  STATE="$STATE  `printf '%-20s' "$LINK"` -> $TARGET (modified $MODTIME)\n"
done <<< "`find $DESTINATION -maxdepth 1 -type l -name \"${RELEASE}*\" -printf '%T+ %P\n' | sort | tail -n 10`"
# grab the last 10 symlinks so we can see the state of the previous point release as well

FROZEN=`find $DESTINATION -maxdepth 1 -type f -name ".freeze.${RELEASE}*" -print -quit`
if [[ -n "$FROZEN" ]]; then
  STATE="$STATE\n ***************************************************\n"
  STATE="$STATE ***  .freeze.${RELEASE}* exists, links won't be updated  ***\n"
  STATE="$STATE ***************************************************\n"
fi
export STATE="`printf "$STATE"`"

# There are no diffs to report on, we can stop
if [[ ! -s "$TMPDIR/.longlist" ]]; then
  if [[ $CHANNEL == "daily" ]]; then
    export NOPKG_SUBJECT_PREFIX="[DIFF]"
  elif [[ $CHANNEL == "testing" ]]; then
    export NOPKG_SUBJECT_PREFIX="[TEST]"
  else
    export NOPKG_SUBJECT_PREFIX="[SECURITY]"
  fi
  # But let's inform the admins (so they don't worry unneccessarily)
  envsubst < /root/templates/email_nopackages.tpl >> "$TMPDIR/.email"
  cat "$TMPDIR/.email" | swaks --server $MAILMX --to $EMAIL_ADMIN --data -
  rm "$TMPDIR/.email"
  exit
fi

# Update website
if [[ $CHANNEL == "production" ]] || [[ $CHANNEL == "testing" ]]; then
  /root/commit-updates-to-docs.sh $TMPDIR $CHANNEL
fi

# Now lets assemble the list of highlights for the email's subject
cat "$TMPDIR/.longlist" | sort -fuo "$TMPDIR/.longlist"
SHORTLIST=`grep -f <(echo -n "$HIGHLIGHT_RPMS" | sed 's/\([^[:space:]]\+\) \?/^\1$\n/g') "$TMPDIR/.longlist" | head -n $SHORTLIST_COUNT | xargs`
LEN_SHORTLIST=`echo "$SHORTLIST" | awk -F" " '{printf "%d",NF}'`
if [[ $LEN_SHORTLIST -lt $SHORTLIST_COUNT ]]; then
  D=`expr $SHORTLIST_COUNT - $LEN_SHORTLIST`
  SHORTLIST="$SHORTLIST `grep -v -f <(echo -n "$HIGHLIGHT_RPMS" | sed 's/\([^[:space:]]\+\) \?/^\1$\n/g') "$TMPDIR/.longlist" | head -n $D | xargs`"
fi
rm "$TMPDIR/.longlist"

echo "Sending email to $CHANNEL users"

export TODAY="$TODAY"
export SHORTLIST="`echo "$SHORTLIST" | sed 's/\b\s\+\b/, /g'`"
export WEBSITE

envsubst < $TEMPLATE | sed '/^--PACKAGES--$/Q' > "$TMPDIR/.email"
for ARCH in $SUPPORTED_ARCHES; do
  for REPO in $ALL_REPOS; do
    CLEANREPO="${REPO/\//-}"
    [ -e "$TMPDIR/.diff:$CLEANREPO:$ARCH" ] || continue

    echo "$REPO $ARCH repository:" >> "$TMPDIR/.email"

    # Let's assemble the summary of packages for this repo
    cat "$TMPDIR/.diff:$CLEANREPO:$ARCH" > "$TMPDIR/.shortlist"
    # LOS-483 Do not include i686 packages in the email
    sed -i '/;i686/d' "$TMPDIR/.shortlist"
    # Remove debuginfo and debugsource packages
    sed -i '/debug\(info\|source\)/d' "$TMPDIR/.shortlist"
    # Remove spammy packages
    sed -i '/glibc-langpack-/d' "$TMPDIR/.shortlist"
    # Always comes with a new kernel anyway
    sed -i '/kernel-\(rt-\)\{0,1\}debug-/d' "$TMPDIR/.shortlist"
    # Docs packages are boring
    sed -i '/-docs/d' "$TMPDIR/.shortlist"
    sed -i '/-man-pages/d' "$TMPDIR/.shortlist"
    # Truncate the list to at most $MAX_PACKAGES_LIST lines
    sed -i ${MAX_PACKAGES_LIST}q "$TMPDIR/.shortlist"

    # Print it nicely formatted in the email
    awk -F';' '{printf "  %-50s %s-%s\n", $2, $3, $4}' "$TMPDIR/.shortlist" >> "$TMPDIR/.email"

    LINES=$(wc -l "$TMPDIR/.diff:$CLEANREPO:$ARCH" | cut -d' ' -f1)
    AFTER=$(wc -l "$TMPDIR/.shortlist" | cut -d' ' -f1)
    if [[ $LINES -gt $AFTER ]]; then
      echo "   [... truncated list (${LINES} packages in total) ...]" >> "$TMPDIR/.email"
    fi
    echo >> "$TMPDIR/.email"
    rm "$TMPDIR/.shortlist"

  done
done
envsubst < $TEMPLATE | sed '1,/^--PACKAGES--$/d' >> "$TMPDIR/.email"
cat "$TMPDIR/.email" | swaks --server $MAILMX --to $EMAIL_TO --data -
rm "$TMPDIR/.email"
